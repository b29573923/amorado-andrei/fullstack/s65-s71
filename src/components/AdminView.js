import { Table } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';

export default function AdminDashboard({productsData, fetchProduct}) {
  const [products, setProducts] = useState([]);

  useEffect(() => {

    const productsArr = productsData.map(product =>{
      console.log(product)
      return (

        <tr key={product._id}>
          <td>{product._id}</td>
          <td>{product.name}</td>
          <td>{product.description}</td>
          <td>{product.price}</td>
          <td className={product.isActive ? 'text-success' : 'text-danger'}>
            {product.isActive ? 'Available' : 'Unavailable'}
          </td>
          <td>
            <img src={product.imgUrl} alt={product.name} style={{width:'100px'}}/>
          </td>
          {/*Course id is passed as a prop*/}
          <td>
            <EditProduct product={product._id} fetchProduct={fetchProduct}/>
          </td>
          <td>
            <ArchiveProduct productId={product._id} isActive={product.isActive} fetchProduct={fetchProduct}/>
          </td>
        </tr>
        )
    })

    setProducts(productsArr)

  }, [productsData, fetchProduct])
  

  return (
    <>
      <h2 className="text-center my-4">Admin Dashboard</h2>
      <Table striped bordered hover responsive>
        <thead className="text-center">
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th colSpan="2">Actions</th>
          </tr>
        </thead>
        <tbody>
          {products}
        </tbody>
      </Table>
    </>
  );
}