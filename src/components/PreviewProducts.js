import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Product(props) {
	console.log(props)
	const { breakPoint, data } = props

	const { _id, name, description, price, imgUrl } = data

	return (

		<Col xs={12} md={breakPoint} className="m-3">
			<Card className="cardHighlight">
			{imgUrl && (
			    <div className ="image-container">
			            <Card.Img src={imgUrl} alt={name} className="product-image p-3"/>
			    </div>
			    )}
				<Card.Body>
					<Card.Title className="text-center">
					<Link to={`/products/${_id}`}>{name}</Link>
					</Card.Title>
					<Card.Text>{description}</Card.Text>
				</Card.Body>

				<Card.Footer>
					<h5 className="text-center text-danger">&#8369;{price}</h5>
					<Link to={`/products/${_id}`} className="btn btn-primary d-block">Details</Link>
				</Card.Footer>
			</Card>

		</Col>

	)
}
