import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights(){
    return (
        <Row className='mt-3 mb-3'>
            <Col xs={12} md={4}>
                <Card className='cardHiglight p-3'>
                    <Card.Body>
                        <Card.Title>Tameable!</Card.Title>
                        <Card.Text>
                        All of our animals are highly tameable and would be a suitable pet for your lovely family.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className='cardHiglight p-3'>
                    <Card.Body>
                        <Card.Title>Free delivery upon purchase.</Card.Title>
                        <Card.Text>
                        Tired of pricey delivery fees? Dont worry, we'll deliver your dinosaur for free! :) 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className='cardHiglight p-3'>
                    <Card.Body>
                        <Card.Title>Need a pet? We got you!</Card.Title>
                        <Card.Text>
                        No need to look further, with just one click we'll bring you your dream pet.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

        </Row>
    )
}