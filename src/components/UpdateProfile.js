// import React, { useState } from 'react';

// const UpdateProfile = ({fetchUserProfile}) => {
//   const [firstName, setFirstName] = useState('');
//   const [lastName, setLastName] = useState('');
//   const [mobileNo, setMobileNo] = useState('');
//   const [message, setMessage] = useState('');
  
//   const handleSubmit = async (e) => {
//     e.preventDefault();
    
//     const token = localStorage.getItem('token'); // Replace this with the actual JWT token
    
//     const data = {
//       firstName,
//       lastName,
//       mobileNo
//     };
    
//     try {
//       const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
//         method: 'PUT',
//         headers: {
//           'Content-Type': 'application/json',
//           'Authorization': `Bearer ${token}`
//         },
//         body: JSON.stringify(data)
//       });
      
//       if (response.ok) {
//         setMessage('Profile udpated successfully');
//         setFirstName('');
//         setLastName('');
//         setMobileNo('');
//         fetchUserProfile();
//       } else {
//         const errorData = await response.json();
//         setMessage(errorData.message);
//       }

//     } catch (error) {
//       setMessage('An error occurred. Please try again.');
//       console.error(error);
//     }
//   };
  
//   return (
//     <div className="container mt-4 my-5">
//       <h3>Update Profile</h3>
//       <form onSubmit={handleSubmit}>
//         <div className="form-group">
//           <label htmlFor="firstName">First Name:</label>
//           <input
//             type="text"
//             className="form-control"
//             id="firstName"
//             value={firstName}
//             onChange={(e) => setFirstName(e.target.value)}
//             required
//           />
//         </div>
//         <div className="form-group">
//           <label htmlFor="lastName">Last Name:</label>
//           <input
//             type="text"
//             className="form-control"
//             id="lastName"
//             value={lastName}
//             onChange={(e) => setLastName(e.target.value)}
//             required
//           />
//         </div>
//         <div className="form-group">
//           <label htmlFor="mobileNo">Mobile Number:</label>
//           <input
//             type="text"
//             className="form-control"
//             id="mobileNo"
//             value={mobileNo}
//             onChange={(e) => setMobileNo(e.target.value)}
//             required
//           />
//         </div>
//         <button type="submit" className="btn btn-primary mt-3">Update Profile</button>
//       </form>
//       {message && <div className="mt-3 alert alert-info">{message}</div>}
//     </div>
//   );
// };

// export default UpdateProfile;
