import { Container, Row, Col } from 'react-bootstrap';
import Banner from '../components/Banner';
import ProductCard from '../components/ProductCard';
import Highlights from '../components/Highlights';
import FeaturedProducts from '../components/FeaturedProducts';

export default function Home() {

    const data = {
        title: "Shozada",
        content: "Shop shop shop shop!",
        destination: "/products",
        label: "Affordable and quality pets!"
    }

    return (
        <>
            <Banner data={data}/>
            <FeaturedProducts/>
            <Highlights/>
            

            
        </>
    )

};