// import coursesData from '../data/coursesData';
import {useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import ProductCard from '../components/ProductCard';
// import AdminView from '../components/AdminView';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import {Row, Col, Card, Button, Container} from 'react-bootstrap';

  

export default function Products() {

  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([])



  const fetchProduct = () => {
    
    // get all active courses
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
    .then(res => res.json())
    .then(data =>{
      
      console.log(data)

      
      setProducts(data);
    });

  }


  // Retrieves the courses from the databasee upon initial render of the "Courses" component
  useEffect(() =>{

    fetchProduct();

  },[]);

  return (
      <Container fluid className ="p-5">
       <Row>
          <Col>
        {user.isAdmin ? (
         
          <AdminView productsData={products} fetchProduct={fetchProduct}/>
        ) : (
          // If the user is not an admin, show the Regular Courses Page.
          <UserView productsData={products} />

        )}
        </Col>
       </Row> 
      </Container>
    );

};