import React, { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductView({ user }) {
  const { productId } = useParams();
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [imgUrl, setImgUrl] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1); // Add state for quantity

  useEffect(() => {
    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        // setImgUrl(data.imgUrl)
      });
  }, [productId]);

  const handleQuantityChange = (event) => {
      setQuantity(parseInt(event.target.value)); // Parse the input value to an integer
    };

  const order = () => {
    // Check if the user is logged in
    if (!user.id) {
      Swal.fire({
        title: 'Not Logged In',
        icon: 'error',
        text: 'Please log in to place an order.',
      });
      return;
    }

    // If the user is logged in, proceed with the order
     fetch(`${process.env.REACT_APP_API_URL}/orders/`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
          body: JSON.stringify({
            userId: user.id,
            products: [{ productId: productId, quantity: quantity }], // Send the selected product and its quantity
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);

            if (data) {
              Swal.fire({
                title: 'Successfully Ordered',
                icon: 'success',
                text: 'You have successfully placed an order.',
              });

              navigate('/products');
            } else {
              Swal.fire({
                title: 'Something Went Wrong',
                icon: 'error',
                text: 'Please try again.',
              });
            }
          });
      };

      return (
      	(user.isAdmin) ?
			<Navigate to="/products"/>
			:
        <Container className="mt-5">
          <Row>
            <Col lg={{ span: 6, offset: 3 }}>
              <Card>
                <Card.Body className="text-center">
                  <Card.Title>{name}</Card.Title>
                  <Card.Subtitle>Description:</Card.Subtitle>
                  <Card.Text>{description}</Card.Text>
                  <Card.Subtitle>Price:</Card.Subtitle>
                  <Card.Text>PhP {price}</Card.Text>
                  <Card.Subtitle>Quantity:</Card.Subtitle>
                  <Card.Text>
                    <input
                      type="number"
                      min="1"
                      value={quantity}
                      onChange={handleQuantityChange}
                    />
                  </Card.Text>
                  {user.id ? (
                    <Button variant="primary" onClick={order}>
                      Order
                    </Button>
                  ) : (
                    <Button as={Link} to="/login" variant="danger">
                      Log in to Order
                    </Button>
                  )}
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      );
    }